import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
Vue.use(require('vue-moment'));
// import VueWebsocket from "vue-websocket";
// Vue.use(VueWebsocket, "/inv");

// wss://ws.blockchain.info/inv
// Vue.config.productionTip = false

// import VueSocketIOExt from 'vue-socket.io-extended'
// import io from 'socket.io-client'
// const socket = io('wss://ws.blockchain.info/inv')
// console.log(socket)
// Vue.use(VueSocketIOExt, socket);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

// import 'vue-draggable-resizable/dist/VueDraggableResizable.css'