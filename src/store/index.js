import Vue from 'vue'
import Vuex from 'vuex'
const baseUrl = process.env.VUE_APP_BASE_URL
const apiKey = process.env.VUE_APP_API_KEY

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cities: JSON.parse(localStorage.getItem('cities')) || [],
    currentCity: null
  },
  getters: {
    cities: (s) => s.cities,
    currentCity: (s) => s.currentCity
  },
  mutations: {
    SET_CITIES (state, data) {
      console.log(state.cities)
      state.cities = data
      console.log(state.cities)
      localStorage.setItem('cities', JSON.stringify(state.cities));
    },
    SET_CITY (state, data) {
      state.cities.push(data)
      localStorage.setItem('cities', JSON.stringify(state.cities));
    },
    SET_CURRENT_CITY (state, data) {
      state.currentCity = data
    },
    UPDATE_CITY (state, data) {
      const item = state.cities.find(c => c.name === data.name)
      Object.assign(item, data);
      localStorage.setItem('cities', JSON.stringify(state.cities));
    },
    ROMOVE_CITY (state, id) {
      const index = state.cities.findIndex(c => c.id === id)
      state.cities.splice(index,1)
      localStorage.setItem('cities', JSON.stringify(state.cities));
    }
  },
  actions: {
    async loadCities({ state, commit }) {
      try {
        const list = ['Penza', 'Perm', 'Novosibirsk', 'Kazan']
        let cities = []
        
        if (state.cities.length == 0) {
          list.map(async (c) => {
            let response = await fetch(`${baseUrl}/data/2.5/weather?q=${c}&appid=${apiKey}`);
            let city = await response.json();
            cities.push(city)
            commit('SET_CITIES', cities);
          })
        }
      } catch (e) {
        throw new Error(e);
      }
    },
    async updateCityByName({ commit }, name) {
      try {
        let response = await fetch(`${baseUrl}/data/2.5/weather?q=${name}&appid=${apiKey}`);
        let city = await response.json();
        if (city.cod == 200) {
          commit('UPDATE_CITY', city)
        }
      } catch (e) {
        throw new Error(e);
      }
    },
    async loadCityByName({ commit }, name) {
      try {
        let response = await fetch(`${baseUrl}/data/2.5/weather?q=${name}&appid=${apiKey}`);
        let city = await response.json();
        if (city.cod == 200) {
          commit('SET_CITY', city)
        }
        return city.cod
        
      } catch (e) {
        throw new Error(e);
      }
    },
    async loadCurrentCity({ commit }) {
      try {
        navigator.geolocation.getCurrentPosition(
          async position => {
            const lat = position.coords.latitude
            const long = position.coords.longitude
            const response = await fetch(`${baseUrl}/data/2.5/weather?lat=${lat}&lon=${long}&appid=${apiKey}`);
            let city = await response.json();
            commit('SET_CURRENT_CITY', city);
          },
          error => {
            console.log(error.message);
          },
        )
        
      } catch (e) {
        throw new Error(e);
      }
    },
  },
  modules: {
  }
})
