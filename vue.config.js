let isProd = process.env.NODE_ENV === 'production';
let path = require('path')
console.log(isProd)
let config = {
	publicPath: isProd ? '' : '/',
	productionSourceMap: false,
	devServer: {
    proxy: {
      '/inv': {
        target: 'wss://ws.blockchain.info',
        ws: true,
        secure: false,
        changeOrigin: true,
        transports: ['websocket', 'polling'],
        allowEIO3: true
      },
    }
  }
};

module.exports = config;

// const io = require('socket.io')(server, {
//   cors: {
//       origin: "http://localhost:8100",
//       methods: ["GET", "POST"],
//       transports: ['websocket', 'polling'],
//       credentials: true
//   },
//   allowEIO3: true
// });